package com.ef.dao;

import com.ef.model.Loganalize;

import java.util.List;

public interface LogRepositoryCustom {

    List<Loganalize> getRequests(String startDate, String duration, Long threshold);
}

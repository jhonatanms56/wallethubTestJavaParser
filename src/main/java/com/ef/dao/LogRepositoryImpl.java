package com.ef.dao;

import com.ef.model.Log;
import com.ef.model.Loganalize;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


import java.math.BigInteger;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LogRepositoryImpl implements LogRepositoryCustom{

    @PersistenceContext
    EntityManager em;



    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    @Transactional(readOnly = true)
    public List<Loganalize> getRequests(String startDate, String duration, Long threshold) {
        Session session = em.unwrap(Session.class);
        Date date = null;
        try {
            date = format.parse(startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("Start Date " + date.toString());

        //Here wie find the List of Ip's
        String sql = "SELECT distinct ip  FROM parserdb.log ";
        String where = "";

        switch (duration){
            case "hourly" :
                where = " where start_date >= :startDate and start_date < DATE_ADD(:startDate,INTERVAL 1 HOUR)  ";
                sql += where;
                break;
            case "daily" :
                where = " where start_date >= :startDate and start_date < DATE_ADD(:startDate,INTERVAL 1 DAY)  ";
                sql += where;
                break;
        }

        NativeQuery query = session.createNativeQuery(sql);
        query.setParameter("startDate",date);
        List<String> listRequestIps = query.list();

        String sql2 = "select count(ip) totalRequest FROM parserdb.log ";
        sql2 += where;
        NativeQuery query2;
        List<Loganalize> listLoganalize = new ArrayList<>();

        for(int i=0;i<=listRequestIps.size()-1;i++){
            String ip = listRequestIps.get(i);
            query2 = session.createNativeQuery(sql2 + " and ip = :ip");
            query2.setParameter("ip",ip);
            query2.setParameter("startDate",date);
            BigInteger bg =  (BigInteger) query2.uniqueResult();
            Long totalRequests = bg.longValue();
            if(totalRequests>=threshold){
                Loganalize loganalize = new Loganalize();
                loganalize.setIp(ip);
                loganalize.setTotal_request(totalRequests);
                loganalize.setDescription("This Ip was Blocked because it made equals or more than " + threshold + " Requests");
                listLoganalize.add(loganalize);

            }
        }


        return listLoganalize;
    }



}

package com.ef.dao;

import com.ef.model.Loganalize;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogAnalizeRepository extends CrudRepository<Loganalize,Long> {
	

}

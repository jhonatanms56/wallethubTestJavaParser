package com.ef.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "log")
public class Log {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private long id;

	@Column(name = "start_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date startDate;

	@Column(name = "duration")
	private String duration;

	@Column(name = "ip")
	private String ip;

	@Column(name = "request_type")
	private String request_type;

	@Column(name = "status")
	private String status;

	@Column(name = "client")
	private String client;

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public String getRequest_type() {
		return request_type;
	}

	public void setRequest_type(String request_type) {
		this.request_type = request_type;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	@Override
	public String toString() {
		return "Log{" +
				"id=" + id +
				", startDate=" + startDate +
				", duration='" + duration + '\'' +
				", ip='" + ip + '\'' +
				", request_type='" + request_type + '\'' +
				", status='" + status + '\'' +
				", client='" + client + '\'' +
				'}';
	}
}

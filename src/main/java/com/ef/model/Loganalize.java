package com.ef.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "loganalize")
public class Loganalize {


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private long id;

	@Column(name = "ip")
	private String ip;

	@Column(name = "total_request")
	private Long total_request;

	@Column(name = "description")
	private String description;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Long getTotal_request() {
		return total_request;
	}

	public void setTotal_request(Long total_request) {
		this.total_request = total_request;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Loganalize{" +
				"ip='" + ip + '\'' +
				", total_request=" + total_request +
				", description='" + description + '\'' +
				'}';
	}
}

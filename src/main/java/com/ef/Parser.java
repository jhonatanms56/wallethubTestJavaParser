package com.ef;


import javax.sql.DataSource;

import com.ef.dao.LogAnalizeRepository;
import com.ef.model.Loganalize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.ef.dao.LogRepository;
import com.ef.model.Log;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

@SpringBootApplication
@EnableJpaRepositories("com.ef.dao")
public class Parser implements CommandLineRunner {

	@Autowired
	DataSource dataSource;

	@Autowired
	LogRepository logRepository;

	@Autowired
	LogAnalizeRepository logAnalizeRepository;

    DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

	public static void main(String[] args) {
		SpringApplication.run(Parser.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Starting Process");
		//readFromFile("D:\\Practice\\access.log");
		if(args.length!=0){
			if(args.length == 1) {
				System.out.println(args[0]);
				readFromFile(args[0]);
				//readFromFile("D:\\Practice\\access.log");
			}
			else{
				String startDate = args[0].replace("."," ");
				System.out.println(startDate);
				System.out.println(args[1]);
				System.out.println(args[2]);
				analyzeRequests(startDate,args[1],args[2]);
				//analyzeRequests("2017-01-01 00:00:00","hourly","200");
			}
		}else{
			System.out.println("Process Finished did not specified any argument !!");
		}



	}

	public void analyzeRequests(String startDate,String duration,String threshold){
		List<Loganalize> listLogAnalize = logRepository.getRequests(startDate,duration,Long.parseLong(threshold));
		logAnalizeRepository.deleteAll();
		logAnalizeRepository.saveAll(listLogAnalize);
		for(Loganalize log :listLogAnalize){
			System.out.println(log.toString());
		}
		System.out.println("Process Finished");
	}

	public void readFromFile(String path) throws IOException {
		logRepository.deleteAll();
		File file = new File(path);
		BufferedReader br = new BufferedReader(new FileReader(file));
		String st;
		while ((st = br.readLine()) != null) {

            String delimiter = Pattern.quote("|");
			String[] obj = st.split(delimiter);
			saveLog(obj);
		}

		System.out.println("Process Finished");
	}

	public void saveLog(String[] obj){
        Log log = new Log();
        Date date = null;
        try {
            date = format.parse(obj[0]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        log.setStartDate(date);
        log.setIp(obj[1]);
        log.setRequest_type(obj[2]);
        log.setStatus(obj[3]);
        log.setClient(obj[4]);
        logRepository.save(log);
    }

}
